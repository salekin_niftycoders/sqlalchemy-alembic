"""add user table

Revision ID: ad114a690e0a
Revises: 
Create Date: 2023-04-12 15:27:08.162754

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ad114a690e0a'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        'user',
        sa.Column('SystemName', sa.Integer(), nullable=False),
        sa.Column('Name', sa.String(), nullable=True),
        sa.PrimaryKeyConstraint('SystemName')
    )


def downgrade() -> None:
    op.drop_table('user')
