from sqlalchemy import Column, Integer, String, text
from sqlalchemy.orm import declarative_base
from config.db_session import get_db

Base = declarative_base()


class User(Base):
    __tablename__ = "user"
    systemName = Column(name="SystemName", type_=Integer, primary_key=True)
    name = Column(name="Name", type_=String)

    def __str__(self):
        return f"systemName: {self.systemName} and name {self.name}"


if __name__ == '__main__':
    db = next(get_db())
    user_obj = User(systemName="1001", name="sa")
    # query = db.query(User).filter(User.systemName == "1001").first()
    # # print(query)
    q = db.execute(text("SELECT * FROM [dbo].[user]"))
    for data in q:
        print(data)
