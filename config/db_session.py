from typing import Generator

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

DATABASE_URL = "mssql+pyodbc://sa:niftycoders@localhost/demo_db?driver=ODBC+Driver+17+for+SQL+Server"

engine = create_engine(DATABASE_URL, pool_pre_ping=True)
Session = sessionmaker(autocommit=False, bind=engine)


def get_db() -> Generator:
    db = Session()
    try:
        yield db
    finally:
        db.close()
